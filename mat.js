jQuery(function() {

	var man_hold_count = 0;
	mat_menu_attach_actions('#mat');
	document.modal = null;
	
	function mat_menu_attach_actions(pattern) {
		jQuery(pattern).find(".title").hover(
			function () {
				if (man_hold_count) {
					jQuery(this).find('[data-action-tag="insert-after"], [data-action-tag="insert-inside"]').removeClass('inactive');
				} else {
					jQuery(this).find('[data-action-tag="insert-after"], [data-action-tag="insert-inside"]').addClass('inactive');
				}
			}
		);
		if (typeof pattern === 'object')
			jQuery(pattern).find(".action").each(function () {
				if (!jQuery(this).hasClass('attached') && !jQuery(this).hasClass('inactive')) {
					jQuery(this).click(function () {
						mat_execute_action(this);
					});
				}
			});
		else
			jQuery(pattern + " .action").each(function () {
				if (!jQuery(this).hasClass('attached')) {
					jQuery(this).click(function () {
						mat_execute_action(this);
					});
				}
			});
	}

	function man_hold_count_update() {
		var n = jQuery('#mat .action[data-action-tag="grabbed"]');
		man_hold_count = n.length;
	}
	
	function mat_execute_action(th) {
		var command = jQuery(th).attr('data-action-tag');
		var obj = jQuery(th).parent().parent().parent();
		var parentCode = 0;
		
		if (jQuery(obj).hasClass('menu') || jQuery(obj).hasClass('menu-item')) {
			parentCode = jQuery(obj).attr('data-mlid');
			if (!!parentCode) ; else parentCode = 0;
		} 
		
		var cmd = {
			command: command,
			plid: parentCode
		};
		
		switch (command) {
		case 'insert-inside':
		case 'insert-after':
			//search grabbed items
			cmd.grabbed = [];
			var i = 0;
			//prepare grab list
			jQuery('#mat .action[data-action-tag="grabbed"]').each(function () {
				jQuery(this)
					.removeClass('context')
					.html('<i class="fa fa-spin fa-refresh" aria-hidden="true"></i>');
				
				var obj_menu = jQuery(this).parent().parent().parent();
				var mlid = jQuery(obj_menu).attr('data-mlid');
				cmd.grabbed[i++] = mlid;
			});
			
			menuname = jQuery(obj).attr('data-name');
			if (!!menuname) ; else menuname = '';
			cmd.menuname = menuname;			
			
			jQuery.ajax(
				{
					type: 'POST',
					data: cmd,
					url: '/admin/structure/menu/tool/ajax',
					success: function (responseText, textStatus, jqXHR ) {
						if (command == 'insert-inside') {
							var a = eval(responseText);
							if (a[0] != 'OK') {
								//unsuccessfull tremination
								document.modal = new tingle.modal();
								document.modal.setContent('<h1>' + a[0] + '</h1>');
								document.modal.open();
								//restore grab status
								jQuery('#mat .action[data-action-tag="grabbed"]')
									.html('<i class="fa fa-hand-rock-o" aria-hidden="true"></i>');
							} else {
								//OK
								jQuery('#mat .action[data-action-tag="grabbed"]').each(function () {
									var obj_menu = jQuery(this).parent().parent().parent();
									obj_menu.html('');
								});
								man_hold_count_update();
								
								var reload_th = jQuery(obj).children('.title').find('.action[data-action-tag="unload"],.action[data-action-tag="load"]')
								.attr('data-action-tag', 'load').get(0);
								
								if (!!reload_th) {
									mat_execute_action(reload_th);
								}								
							}
						}
						
						if (command == 'insert-after') {
							var a = eval(responseText);
							if (a[0] != 'OK') {
								//unsuccessfull tremination
								document.modal = new tingle.modal();
								document.modal.setContent('<h1>' + a[0] + '</h1>');
								document.modal.open();
								//restore grab status
								jQuery('#mat .action[data-action-tag="grabbed"]')
									.html('<i class="fa fa-hand-rock-o" aria-hidden="true"></i>');
							} else {
								//OK
								jQuery('#mat .action[data-action-tag="grabbed"]').each(function () {
									var obj_menu = jQuery(this).parent().parent().parent();
									obj_menu.html('');
								});
								man_hold_count_update();
								
								if (jQuery(obj).hasClass('menu'))
									var obj_parent = obj;
								else
									var obj_parent = jQuery(obj).parents('.menu-item, .menu').get(0);

								var reload_th = jQuery(obj_parent).children('.title').find('.action[data-action-tag="unload"],.action[data-action-tag="load"]')
								.attr('data-action-tag', 'load').get(0);
								
								if (!!reload_th) {
									mat_execute_action(reload_th);
								}								
							}
						}						
					}
				}
			);				
			
			break;
		case 'delete':
			document.modal = new tingle.modal({
				footer: true,
			});
			document.modal.setContent('<h1>Do you really want to delete the menu item?</h1>');
			document.modal.addFooterBtn('Delete', 'tingle-btn tingle-btn--danger', function() {
				document.modal.close();
				
				jQuery(th).removeClass('context')
					.html('<i class="fa fa-spin fa-refresh" aria-hidden="true"></i>');
				cmd.recurse = 0;
				jQuery.ajax(
					{
						type: 'POST',
						data: cmd,
						url: '/admin/structure/menu/tool/ajax',
						success: function (responseText, textStatus, jqXHR ) {
							jQuery(obj).html('');
						}
					}
				);				
			});
			document.modal.addFooterBtn('Delete recursivly', 'tingle-btn tingle-btn--danger', function() {
				document.modal.close();
				
				jQuery(th).removeClass('context')
					.html('<i class="fa fa-spin fa-refresh" aria-hidden="true"></i>');

				cmd.recurse = 1;
				jQuery.ajax(
					{
						type: 'POST',
						data: cmd,
						url: '/admin/structure/menu/tool/ajax',
						success: function (responseText, textStatus, jqXHR ) {
							jQuery(obj).html('');
						}
					}
				);
			});
			document.modal.addFooterBtn('Cancel', 'tingle-btn tingle-btn--primary', function() {
				document.modal.close();
			});
			
			document.modal.open();			
		
			break;
		case 'new':
			if (jQuery(obj).hasClass('menu') || jQuery(obj).hasClass('menu-item')) {
				menuname = jQuery(obj).attr('data-name');
				if (!!menuname) ; else menuname = '';
				
				document.modal = new tingle.modal({
					onClose: function() {
						var reload_th = jQuery(obj).children('.title').find('.action[data-action-tag="unload"],.action[data-action-tag="load"]')
						.attr('data-action-tag', 'load').get(0);
						
						if (!!reload_th) {
							mat_execute_action(reload_th);
						}								

					},
				});
				document.modal.setContent('<iframe src="/admin/structure/menu/tool/modal/new?menuname=' + menuname + '&plid=' + parentCode + '&destination=admin/structure/menu/tool/modal/close" width="100%" style="height: 700px;"></iframe>');
				document.modal.open();
			}
			
			break;
		case 'edit':
			if (parentCode > 0) {
				
				document.modal = new tingle.modal();
				document.modal.setContent('<iframe src="/admin/structure/menu/item/' + parentCode + '/edit?destination=admin/structure/menu/tool/modal/close" width="100%" style="height: 700px;"></iframe>');
				document.modal.open();
			}
			break;
		case 'hidden':
			if (cmd.plid) {

				jQuery(th).removeClass('context')
					.html('<i class="fa fa-spin fa-refresh" aria-hidden="true"></i>');

				jQuery.ajax(
					{
						type: 'POST',
						data: cmd,
						url: '/admin/structure/menu/tool/ajax',
						success: function (responseText, textStatus, jqXHR ) {
							jQuery(th).attr('data-action-tag', 'visible')
								.addClass('context')
								.html('<i class="fa fa-eye" aria-hidden="true"></i>');
						}
					}
				);			
			}		
		
			break;
		case 'visible':
			if (cmd.plid) {
		
				jQuery(th).removeClass('context')
					.html('<i class="fa fa-spin fa-refresh" aria-hidden="true"></i>');

				jQuery.ajax(
					{
						type: 'POST',
						data: cmd,
						url: '/admin/structure/menu/tool/ajax',
						success: function (responseText, textStatus, jqXHR ) {
							jQuery(th).attr('data-action-tag', 'hidden')
								.removeClass('context')
								.html('<i class="fa fa-eye-slash" aria-hidden="true"></i>');
						}
					}
				);			
			}
			break;
		case 'grab':
			jQuery(th).attr('data-action-tag', 'grabbed')
				.removeClass('context')
				.html('<i class="fa fa-hand-rock-o" aria-hidden="true"></i>');
			man_hold_count ++;
			break;
		case 'grabbed':
			jQuery(th).attr('data-action-tag', 'grab')
				.addClass('context')
				.html('<i class="fa fa-hand-paper-o" aria-hidden="true"></i>');
			man_hold_count --;
			break;			
		case 'unload':
			jQuery(obj).find('.content .tools').hide();
			jQuery(obj).children('.content').slideUp(500).html('');
			jQuery(th).attr('data-action-tag', 'load');
			jQuery(th).html('<i class="fa fa-level-down" aria-hidden="true"></i>');
			man_hold_count_update();
			break;
		case 'load':
			
			if (jQuery(obj).hasClass('menu') || jQuery(obj).hasClass('menu-item')) {
				
				menuname = jQuery(obj).attr('data-name');
				if (!!menuname) ; else menuname = '';
				cmd.menuname = menuname;
				jQuery(obj).children('.content').show().addClass('loading').load(
					'/admin/structure/menu/tool/ajax',
					cmd,
					function (responseText, textStatus, jqXHR ) {
						jQuery(th).attr('data-action-tag', 'unload');
						jQuery(th).html('<i class="fa fa-level-up" aria-hidden="true"></i>');
						
						jQuery(this).removeClass('loading');
						mat_menu_attach_actions(this);
						man_hold_count_update();
					}
				);
			}
		}
	}
	

	
});