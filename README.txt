Menu Administration Tool
------------------------

The Menu Administration Tool module allows you alternative way to edit menu on administration page, adding another one menu manager besides you already have in Drupal. The tool is trying to solve some problems inherent in the basic menu editor.

When you have a huge menu with hundreds items, original admin menu editor (admin/structure/menu) becomes slow and expensive in a terms of memory consumption. It uses the big html form with a lot of variables (hidden inputs and ect) which can over-limits your max_input_vars php variable. Reattaching menu branch from one place to another, trying scrolling the menu and keeping the branch in a mouse, is a real torture. 

My solution is to use ajax partial loading to work only with necessary branches, update only necessary points, using modal boxes to edit and to add new items.

Dependencies
------------
 * menu
 
Install
-------
Install the Menu Administration Tool module.

1) Copy the mat folder to the modules folder in your installation.

2) Enable dependence if not yet (core menu module).

3) Enable the Menu Administration Tool module using Administer -> Modules (/admin/modules).
